variable "adminProjectId" {
  type = string
  description = "Project Id of the administrator project used to generate the others"
}

variable "region" {
  type = string
  description = "Region used to create the projects"
}

variable "billing_account" {
  type = string
  description = "Billing Account"
}

variable "admins_lit" {
  type = list(string)
  description = "Emails list of the administrator of the project"
}

variable "trainers_list" {
  type = list(string)
  default = []
  description = "Emails list of the trainers for the projects"
}
