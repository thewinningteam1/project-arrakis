
module "graduates_projects" {
  for_each = local.graduates_per_team

  source = "./modules/graduates-project-generator"
  admin_project_id = var.adminProjectId
  region = var.region
  billing_account = var.billing_account

  project_name = each.key

  admins = var.admins_lit
  trainers-list = []

  graduates_list = each.value

  depends_on = [module.shared_project_update.db-credentials-user-list]
}

resource "null_resource" "project_id_per_team" {
  for_each = local.graduates_per_team

  provisioner "local-exec" {
    command = "echo ${module.graduates_projects[each.key]}  >> components-of-team-${each.key}.txt"
  }

}
