
resource "google_sourcerepo_repository" "repo" {
  for_each = toset(var.repos_list)
  name     = each.key
  project  = google_project.new_project.project_id
}

data "google_iam_policy" "writers" {
  binding {
    role = "roles/source.writer"
    members = local.graduates_emails_as_users_members_list
  }
}

resource "google_sourcerepo_repository_iam_policy" "policy" {
  for_each = toset(var.repos_list)

  project = google_project.new_project.project_id
  repository = google_sourcerepo_repository.repo[each.key].name
  policy_data = data.google_iam_policy.writers.policy_data
}
