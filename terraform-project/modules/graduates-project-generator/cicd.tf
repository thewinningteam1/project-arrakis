resource "google_project_service_identity" "build_sa" {
  provider = google-beta
  project  = google_project.new_project.project_id
  service  = "cloudbuild.googleapis.com"
}

locals {
  build_roles = [
    "roles/appengine.appViewer",
    "roles/appengine.codeViewer",
    "roles/appengine.deployer",
    "roles/cloudbuild.builds.builder"
  ]
}

resource "google_project_iam_member" "build_sa_member_project" {
  for_each = toset(local.build_roles)
  project  = google_project.new_project.project_id
  member   = "serviceAccount:${google_project_service_identity.build_sa.email}"
  role     = each.key
}

resource "google_cloudbuild_trigger" "trigger_master" {
  for_each = toset(var.repos_list)
  project  = google_project.new_project.project_id

  name     = "${google_sourcerepo_repository.repo[each.key].name}-master-branch-trigger"
  filename = "cloudbuild.yaml"

  trigger_template {
    branch_name = "^master$"
    repo_name = google_sourcerepo_repository.repo[each.key]
  }
}

resource "google_cloudbuild_trigger" "trigger_feature" {
  for_each = toset(var.repos_list)
  project  = google_project.new_project.project_id

  name     = "${google_sourcerepo_repository.repo[each.key].name}-feature-branch-trigger"
  filename = "cloudbuild-features.yaml"

  trigger_template {
    branch_name = "features/*"
    repo_name = google_sourcerepo_repository.repo[each.key]
  }
}

