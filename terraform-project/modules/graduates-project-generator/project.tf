resource "google_project" "new_project" {
  name            = var.project_name
  project_id      = local.project_id
  billing_account = data.google_billing_account.account_number.id
}

resource "google_project_service" "project" {
  depends_on = [google_project.new_project]
  for_each = concat(local.default_api_to_enable, var.activate_api)
  project = google_project.new_project.id
  service = each.key

  timeouts {
    create = "30m"
    update = "40m"
  }

  disable_dependent_services = true
}


