resource "google_app_engine_application" "app-engine" {
  project     = google_project.new_project.project_id
  location_id = var.region
}

resource "google_app_engine_firewall_rule" "rule" {
  project      = google_app_engine_application.app-engine.project
  priority     = 1000
  action       = "ALLOW"
  source_range = "*"
}
