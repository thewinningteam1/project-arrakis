resource "google_pubsub_topic" "billing_topic" {
  project = data.google_project.admin_project.project_id
  name = "billing_topic"
}
