variable "project_name" {
  type = string
  description = "Project Name"
}

variable "admin_project_id" {
  type = string
  description = "Shared project Id"
}

variable "billing_account" {
  type = string
  description = ""
}

variable "region" {
  type = string
  description = "Region used to create the projects"
}

variable "graduates_list" {
  type = list(string)
  description = "Graduates details for the given Project"
}

variable "admins" {
  type = list(string)
  description = "List of admins emails"
}

variable "trainers-list" {
  type = list(string)
  description = "List of trainers emails"
}

variable "activate_api" {
  type = list(string)
  description = "List of other APIs to enable"
  default = []
}

variable "repos_list" {
  type = list(string)
  description = "List of Repositories"
  default = [
    "arrakis"
  ]
}

