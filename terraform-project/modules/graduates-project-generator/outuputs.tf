output "project_id" {
  value = google_project.new_project.project_id
}

output "repo_url" {
  value = google_sourcerepo_repository.repo[*].url
}

output "appengine_name" {
  value = google_app_engine_application.app-engine.name
}
