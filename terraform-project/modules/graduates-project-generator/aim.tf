locals {
  graduates_emails_as_users_members_list = toset([for graduate-email in var.graduates_list: "user:${graduate-email}"])
}

resource "google_project_iam_binding" "dev_members_viewer" {
  project = google_project.new_project.project_id
  members = local.graduates_emails_as_users_members_list
  role = "roles/viewer"
}

resource "google_project_iam_member" "trainers" {
  for_each = toset(var.trainers-list)
  member = "user:${each.key}"
  role = "roles/editor"
}

resource "google_project_iam_member" "admins_permissions" {
  for_each = toset(var.admins)
  member = "user:${each.key}"
  role = "roles/owner"
}


