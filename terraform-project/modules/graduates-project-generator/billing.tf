data "google_billing_account" "account_number" {
  billing_account = var.billing_account
}

resource "google_billing_budget" "project_budget" {
  billing_account = data.google_billing_account.account_number.id
  display_name = "Graduates-${local.project_id}"
  amount {
    specified_amount {
      currency_code = "EUR"
      units = "100"
    }
  }

  threshold_rules {
    threshold_percent = 0.5
  }

  threshold_rules {
    threshold_percent = 0.7
  }

  threshold_rules {
    threshold_percent = 1
    spend_basis = "FORECASTED_SPEND"
  }

  budget_filter {
    projects = [
      "projects/${google_project.new_project.number}"]
  }

  all_updates_rule {
    pubsub_topic = google_pubsub_topic.billing_topic.id

    monitoring_notification_channels = tolist(google_monitoring_notification_channel.notification_channels_admins[*].id)

    disable_default_iam_recipients = true
  }

  depends_on = [google_monitoring_notification_channel.notification_channels_admins]
}

resource "google_monitoring_notification_channel" "notification_channels_admins" {
  for_each = toset(var.admins)
  display_name = "Budget Monitoring Notification Channel"
  type         = "email"

  labels = {
    email_address = each.key
  }
}
