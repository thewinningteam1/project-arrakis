locals {
  project_id = replace(lower(var.project_name), "([[:punct:]]|[[:space:]])", "-")
  default_api_to_enable =[
    "monitoring.googleapis.com",
    "logging.googleapis.com",
    "cloudbuild.googleapis.com",
    "cloudshell.googleapis.com",
    "serviceusage.googleapis.com",
    "appengine.googleapis.com",
    "websecurityscanner.googleapis.com"
  ]
}

data "google_project" "admin_project" {
  project_id = var.admin_project_id
}
