resource "google_sql_database_instance" "graduates_sql-instance" {
  name = "graduates-instance"
  database_version = "MYSQL_8_0"
  settings {
    tier = "db-g1-small"
  }
}

resource "random_password" "admin-pwd" {
  length           = 10
  special          = true
  override_special = "_%@"
}

resource "google_sql_user" "credentials_admin" {
  instance = google_sql_database_instance.graduates_sql-instance.id
  name = "admin"
  password = random_password.admin-pwd.result
}
