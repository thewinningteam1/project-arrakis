output "db-instance-ip" {
  value = google_sql_database_instance.graduates_sql-instance.public_ip_address
}

output "db-instance-admin-pwd" {
  sensitive = true
  value = google_sql_user.credentials_admin.password
}

output "db-databases-list" {
  value = mysql_database.databases_teams[*].name
}

output "db-credentials-user-list" {
  value = mysql_user.teams_user[*].user
}

output "db-credentials-pwd-list" {
  sensitive = true
  value = mysql_user.teams_user[*].password
}
