terraform {
  required_providers {
    mysql = {
      source = "winebarrel/mysql"
      version = "1.10.4"
    }
  }
}

provider "google" {
  project = var.admin-project-id
  region = var.region
}

provider "google-beta" {
  project = var.admin-project-id
  region = var.region
}
