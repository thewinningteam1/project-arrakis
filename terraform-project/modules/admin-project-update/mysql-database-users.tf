provider "mysql" {
  endpoint = "${google_sql_database_instance.graduates_sql-instance.public_ip_address}:3306"
  username = google_sql_user.credentials_admin.name
  password = google_sql_user.credentials_admin.password
}

resource "mysql_database" "databases_teams" {
  for_each = toset(var.teams-list)
  name = "bonds-${each.key}"
  depends_on = [google_sql_database_instance.graduates_sql-instance]
}

resource "random_password" "teams_pwd" {
  for_each = toset(var.teams-list)

  length           = 10
  special          = true
  override_special = "_%@"
}

resource "mysql_user" "teams_user" {
  depends_on = [google_sql_database_instance.graduates_sql-instance]

  for_each = toset(var.teams-list)
  user = "team-${each.key}"
  plaintext_password = random_password.teams_pwd[each.key].result
  //TODO: provisioners
}

resource "mysql_grant" "grant_access_to_users_per_db" {
  depends_on = [google_sql_database_instance.graduates_sql-instance]

  for_each = toset(var.teams-list)
  database = mysql_database.databases_teams[each.key]
  user = mysql_user.teams_user[each.key]
  privileges = [
    "ALTER",
    "CREATE",
    "CREATE TEMPORARY TABLES",
    "CREATE_VIEW",
    "DELETE",
    "DROP",
    "EVENT",
    "EXECUTE",
    "INDEX",
    "INSERT",
    "LOCK TABLES",
    "SELECT",
    "SHOW VIEW",
    "TRIGGER",
    "UPDATE"
  ]
}
