variable "teams-list" {
  type = list(string)
  description = "List of Teams"
}

variable "admin-project-id" {
  type = string
  description = "Project ID"
}

variable "region" {
  type = string
  description = "Region within to create resources"
}

variable "admins-list" {
  type = list(string)
  description = "List of admins emails"
}

