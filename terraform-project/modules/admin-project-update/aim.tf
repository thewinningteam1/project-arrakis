resource "google_project_iam_member" "admins_permissions" {
  for_each = toset(var.admins-list)
  member = "user:${each.key}"
  role = "roles/owner"
}

