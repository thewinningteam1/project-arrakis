provider "terraform" {
}

provider "google" {
  version = ">= 3.76.0"
}

provider "google-beta" {
  version = ">= 3.76.0"
}

locals {
  json_graduates_list = csvdecode(file("graduates_list.csv"))
  graduates_per_team = {
      for entry in local.json_graduates_list : entry.team_name => entry.email ...
  }
  teams_list = [for team_name, graduates_list in local.graduates_per_team : team_name]
}
resource "null_resource" "test" {
  for_each = local.graduates_per_team

  provisioner "local-exec" {
    command = "echo ${join("\n", each.value)} >> components-of-team-${each.key}.txt"
  }
}
