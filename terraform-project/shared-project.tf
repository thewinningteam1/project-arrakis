module "shared_project_update" {
  source = "./modules/admin-project-update"
  admin-project-id = var.adminProjectId
  admins-list = var.admins_lit
  teams-list = local.teams_list
  region = var.region
}

resource "null_resource" "database_info_per_team" {
  for_each = toset(local.teams_list)

  provisioner "local-exec" {
    command = "echo ${module.shared_project_update[each.key]}  >> database-info-of-team-${each.key}.txt"
  }

}
