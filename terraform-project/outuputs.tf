output "graduates_per_projects" {
  value = local.graduates_per_team
}

output "teams_list" {
  value = local.teams_list
}
