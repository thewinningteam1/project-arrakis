import React, { useState } from 'react';
import Routes from './containers/Routes';
import NavBar from './components/Navbar';
import { AuthProvider } from './context/AuthContext';

const App = () => {

  const [businessDay, setBusinessDay] = useState({date:"07/04/2021", urgency: 'medium'});
  const [maturityDate, setMaturityDate] = useState("07/04/2021");
  const [bondType, setBondType] = useState("");

  return (
      <AuthProvider>
      <NavBar />
      <Routes businessDay = {businessDay} setBusinessDay = {setBusinessDay} maturityDate={maturityDate} setMaturityDate={setMaturityDate} bondType={bondType} setBondType={setBondType}/>
      </AuthProvider>
  );
};

export default App;
