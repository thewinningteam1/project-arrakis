"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var allBondData = [{
  id: 1,
  isin: 'X124567881',
  cusip: '123456789',
  type: 'Sovereign',
  issuer: 'Bundesbank',
  client: 'UBS',
  maturity: '08/02/2030',
  notional: 1000,
  currency: 'EUR',
  coupon: 2.0,
  quantity: 30,
  unitPrice: 900,
  status: 'Active',
  bookId: 'MARTWIN'
}, {
  id: 2,
  isin: 'US124567881',
  cusip: '',
  type: 'Corporate',
  issuer: '3i PLC',
  client: 'Tesco',
  maturity: '09/03/2028',
  notional: 1000,
  currency: 'GBP',
  coupon: 6.75,
  quantity: 30,
  unitPrice: 900,
  status: 'Active',
  bookId: 'ARMLTT'
}, {
  id: 3,
  isin: 'GB124567881',
  cusip: '345678912',
  type: 'Municipal',
  issuer: 'London',
  client: 'Volkswagen',
  maturity: '22/08/2020',
  notional: 1000,
  currency: 'GBP',
  coupon: 3.52,
  quantity: 30,
  unitPrice: 900,
  status: 'Inactive',
  bookId: 'LENJON'
}, {
  id: 4,
  isin: 'US45906M2L46',
  cusip: '456789123',
  type: 'Supranational',
  issuer: 'IBRD',
  client: 'Pension Fund',
  maturity: '22/10/2021',
  notional: 1000,
  currency: 'TRY',
  coupon: 0.55,
  quantity: 30,
  unitPrice: 900,
  status: 'Active',
  bookId: 'ARMLTT'
}, {
  id: 5,
  isin: 'US124567881',
  cusip: '567891234',
  type: 'Agency',
  issuer: 'FNMA',
  client: 'Citibank',
  maturity: '01/07/2021',
  notional: 1000,
  currency: 'USD',
  coupon: 0.8,
  quantity: 30,
  unitPrice: 900,
  status: 'Active',
  bookId: 'LENJON'
}];
var _default = allBondData;
exports["default"] = _default;