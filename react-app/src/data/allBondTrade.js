const allBondTradeData = [
    {
      id: 1,
      isin: 'X124567881',
      cusip: '123456789',
      type: 'Sovereign',
      issuer: 'Bundesbank',
      maturity: '30/03/2021',
      notional: 1000,
      currency: 'EUR',
      coupon: 2.0,
      trades: [
        {
            client: 'UBS',
            currency: 'EUR',
            status: 'open',
            quantity: 20,
            unitPrice: 500,
            bookId: 'MARTWIN',
            buySell: 'Buy',
            tradeDate: '05/02/2021',
            settlementDate: '05/03/2021'
         },
         {
            client: 'Citibank',
            currency: 'EUR',
            status: 'open',
            quantity: 20,
            unitPrice: 900,
            bookId: 'LENJON',
            buySell: 'Buy',
            tradeDate: '05/02/2021',
            settlementDate: '05/03/2021'
          },
          {
            client: 'Bank of America',
            currency: 'EUR',
            status: 'open',
            quantity: 30,
            unitPrice: 900,
            bookId: 'MARTWIN',
            buySell: 'Sell',
            tradeDate: '25/02/2021',
            settlementDate: '25/03/2021'
          }
      ]
    },
    {
      id: 2,
      isin: 'US124567881',
      cusip: '',
      type: 'Corporate',
      issuer: '3i PLC',
      maturity: '31/03/2021',
      notional: 1000,
      currency: 'USD',
      coupon: 6.75,
      trades: [
        {
            client: 'Tesco',
            currency: 'USD',
            status: 'open',
            quantity: 30,
            unitPrice: 500,
            bookId: 'ARMLTT',
            buySell: 'Buy',
            tradeDate: '10/01/2021',
            settlementDate: '10/02/2021'
          },
          {
            client: 'Citibank',
            currency: 'USD',
            status: 'closed',
            quantity: 14,
            unitPrice: 1000,
            bookId: 'LENJON',
            buySell: 'Sell',
            tradeDate: '10/05/2021',
            settlementDate: '10/06/2021',
          }
      ]
    },
    {
      id: 3,
      isin: 'GB124567881',
      cusip: '345678912',
      type: 'Municipal',
      issuer: 'London',
      maturity: '30/03/2021',
      notional: 27000,
      currency: 'GBP',
      coupon: 3.52,
      trades:[
        {
            client: 'Volkswagen',
            currency: 'GBP',
            status: 'open',
            quantity: 30,
            unitPrice: 900,
            bookId: 'LENJON',
            buySell: 'Buy',
            tradeDate: '10/02/2021',
            settlementDate: '10/03/2021',
          }
      ]
    },
    {
      id: 4,
      isin: 'US45906M2L46',
      cusip: '456789123',
      type: 'Supranational',
      issuer: 'IBRD',
      maturity: '22/10/2021',
      notional: 15000,
      currency: 'TRY',
      coupon: 0.55,
      trades:[
        {
            client: 'Pension Fund',
            currency: 'TRY',
            status: 'open',
            quantity: 30,
            unitPrice: 500,
            bookId: 'ARMLTT',
            buySell: 'Buy',
            tradeDate: '10/08/2021',
            settlementDate: '10/09/2021',
          }
      ]
    },
    {
      id: 5,
      isin: 'US784567888',
      cusip: '567891234',
      type: 'Agency',
      issuer: 'FNMA',
      client: 'Citibank',
      maturity: '01/07/2021',
      notional: 1000,
      currency: 'USD',
      coupon: 0.8,
      trades:[
        {
            client: 'Citibank',
            currency: 'USD',
            status: 'open',
            quantity: 30,
            unitPrice: 600,
            bookId: 'LENJON',
            buySell: 'Buy',
            tradeDate: '01/04/2021',
            settlementDate: '01/05/2021',
          },
    
          {
            client: 'Credit Suisse',
            currency: 'USD',
            status: 'open',
            quantity: 40,
            unitPrice: 1000,
            bookId: 'LENJON',
            buySell: 'Buy',
            tradeDate: '03/04/2021',
            settlementDate: '03/05/2021',
          },
    
          {
            client: 'HSBC',
            currency: 'USD',
            status: 'open',
            quantity: 30,
            unitPrice: 900,
            bookId: 'LENJON',
            buySell: 'Sell',
            tradeDate: '07/04/2021',
            settlementDate: '07/05/2021',
          },
    
          {
            client: 'BNP Paribas',
            currency: 'USD',
            status: 'open',
            quantity: 10,
            unitPrice: 3000,
            bookId: 'LENJON',
            buySell: 'Sell',
            tradeDate: '07/04/2021',
            settlementDate: '07/05/2021',
          }
      ]
    }
];
export default allBondTradeData;