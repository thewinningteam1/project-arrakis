import React, { useContext, useState, useEffect } from 'react';
import firebase from '../firebase/config';

export const AuthContext = React.createContext();

export const useAuth = () => {
  return useContext(AuthContext);
};

export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState('');
  const [loading, setLoading] = useState(true);

  const signUp = (email, password) => {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  };

  const logIn = (email, password) => {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  };

  const logOut = () => {
    return firebase.auth().signOut();
  };

  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      setCurrentUser(user);
      setLoading(false);
    });

    return unsubscribe;
  }, []);

  const value = {
    currentUser,
    logIn,
    signUp,
    logOut,
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};
