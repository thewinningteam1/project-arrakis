import firebase from 'firebase';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyAgZbeXOHQ-dYx-_5sy5rEfCtAqYmaotFg',
  authDomain: 'db-arrakis.firebaseapp.com',
  projectId: 'db-arrakis',
  storageBucket: 'db-arrakis.appspot.com',
  messagingSenderId: '448669335651',
  appId: '1:448669335651:web:a0533e66bfaa121b2e3856',
};

firebase.initializeApp(firebaseConfig);
firebase.auth();

export default firebase;
