import React from 'react';
import styles from './DayCardHeader.module.scss';
import { navigate } from "@reach/router";

const DayCardHeader = (props) => {

  const { maturityDate, setMaturityDate, setBondType, businessDay } = props;
  const headerStyle = [styles.dayCardHeader, styles[businessDay.urgency]];

  const handleLink = () =>  {
    setMaturityDate(maturityDate);
    setBondType("");
    navigate('/dashboard');
    console.log(businessDay.date);
  };

  return (

    <div onClick={handleLink} className = {headerStyle.join(" ")}>
      <h6> {businessDay.displayDate}</h6>
    </div>

  );
};

export default DayCardHeader;
