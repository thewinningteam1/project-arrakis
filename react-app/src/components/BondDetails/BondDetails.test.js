import React from 'react';
import { shallow } from 'enzyme'
import BondDetails from './BondDetails';
import bonds from '../../data/allBondData';

describe('BondDetails tests', () => {
 let component;
 let bondDetails;
 let componentWithoutCusip;
 let bondDetailsWithoutCusip;


 beforeEach(() => {
  bondDetails = bonds[0];
  bondDetailsWithoutCusip = bonds[1];
  component = shallow(<BondDetails bond={bondDetails} />);
})

it('should render', () => {
  expect(component).toBeTruthy();
});

it('should render 1 table header', () => {
  expect(component.find('th').length).toEqual(2);
})

it('should render 10 table rows', () => {
  expect(component.find('td').length).toEqual(20);
})

it('should render text ISIN', () => {
  expect(component.text().includes('ISIN')).toBe(true);
})

it('should render text CUSIP', () => {
  expect(component.text().includes('CUSIP')).toBe(true);
})

it('should render text Issuer', () => {
  expect(component.text().includes('Issuer')).toBe(true);
})

it('should render text Client name', () => {
  expect(component.text().includes('Client name')).toBe(true);
})

it('should render text Maturity date', () => {
  expect(component.text().includes('Maturity date')).toBe(true);
})

it('should render text Bond type', () => {
  expect(component.text().includes('Bond Type')).toBe(true);
})

it('should render text Coupon(%)', () => {
  expect(component.text().includes('Coupon(%)')).toBe(true);
})

it('should render text Quantity', () => {
  expect(component.text().includes('Quantity')).toBe(true);
})

it('should render text Unit Price', () => {
  expect(component.text().includes('Unit Price')).toBe(true);
})

it('should render text EUR', () => {
  expect(component.text().includes('EUR')).toBe(true);
})

it('should render N/A if Cusip value is not applicable', () => {
  componentWithoutCusip = shallow(<BondDetails bond={bondDetailsWithoutCusip} />);
  expect(componentWithoutCusip.text().includes('N/A')).toBe(true);
})





// const wrapper = shallow(<div><button className='btn btn-primary'>OK</button></div>);
// const button = wrapper.find('.btn');
// expect(button.text()).to.be.eql('OK');


})
