import React from 'react';
import styles from './BondDetails.module.scss';

const BondDetails = (props) => {
  return (
    <div className={styles.bondDetails}>
    <div className={styles.closeButton}>
      <div onClick={props.handleClose}>X</div>
    </div>
      <table>
          <tr>
            <th>Bond Details:</th>
            <th>{props.bond.id}</th>
          </tr>
          <br/>
          <tr>
            <td className={styles.heading}>ISIN:</td>
            <td className={styles.content}>{props.bond.isin}</td>
          </tr>
          <tr>
            <td className={styles.heading}>CUSIP:</td>
            <td className={styles.content}>{props.bond.cusip || "N/A"}</td>
          </tr>
          <br/>
          <tr>
            <td className={styles.heading}>Bond Type:</td>
            <td className={styles.content}>{props.bond.type}</td>
          </tr>
          <tr>
            <td className={styles.heading}>Issuer:</td>
            <td className={styles.content}>{props.bond.issuer}</td>
          </tr>

          <br/>
          <tr>
            <td className={styles.heading}>Maturity date:</td>
            <td className={styles.content}>{props.bond.maturity}</td>
          </tr>
          <tr>
            <td className={styles.heading}>Notional:</td>
            <td className={styles.content}>{`${props.bond.notional} ${props.bond.currency}`}</td>
          </tr>
          <tr>
            <td className={styles.heading}>Coupon(%):</td>
            <td className={styles.content}>{props.bond.coupon}</td>
          </tr>
          <br/>
          <tr>
            <td className={styles.heading}>Client name:</td>
            <td className={styles.content}>{props.bond.client}</td>
          </tr>
          <tr>
            <td className={styles.heading}>Quantity:</td>
            <td className={styles.content}>{props.bond.quantity}</td>
          </tr>
          <tr>
            <td className={styles.heading}>Unit Price:</td>
            <td className={styles.content}>{`${props.bond.unitPrice} ${props.bond.currency}`}</td>
          </tr>
          <br/>
      </table>
      <br/>
      <br/>
    </div>
  )
}

export default BondDetails;
