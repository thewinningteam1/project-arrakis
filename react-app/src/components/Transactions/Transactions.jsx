import React, { useState, useEffect }  from 'react';
import styles from './Transactions.module.scss';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { findTradesForSecurity } from "../../services/BondServices";

const Transactions = (props) => {
  const isTransactionOpen = props.id != '' ? styles.open : styles.closed;
  const onGridReady = (params) => {
    params.api.sizeColumnsToFit();
  };

  const valueFormatter = (field) => (field.value == '' ? 'N/A' : field.value);

  const filterDateParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );
      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }
      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }
      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
  };

  const getStatusStyle = (params) => {
    if (params.data.status === 'open') {
      return { color: 'green' };
    }
    if (params.data.status === 'closed') {
      return { color: 'red' };
    }
  };

  const [trades, setTrades] = useState([]);

  useEffect(() => {
    if(props.id)
      findTradesForSecurity(props.id)
        .then(trades => setTrades(trades.data));
  }, [props.id]);

  return (
    <>
      <div className={styles.container} className={isTransactionOpen}>
        <p className={styles.transactionHeader}>Transactions by ISIN</p>
        <div className='ag-theme-alpine' style={{ height: 300, width: 1300 }}>
          <AgGridReact
            style={{ width: '100%', height: '100%;' }}
            pagination={true}
            paginationPageSize={10}
            onGridReady={onGridReady}
            rowData={trades}
          >
            <AgGridColumn
              resizable={true}
              field='isin'
              width={120}
              filter={true}
              valueFormatter={valueFormatter}
              headerName='ISIN'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='bookName'
              width={140}
              filter={true}
              valueFormatter={valueFormatter}
              headerName='Book Id'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='counterpartyName'
              width={100}
              valueFormatter={valueFormatter}
              filter={true}
              headerName='Client'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='status'
              width={120}
              valueFormatter={valueFormatter}
              cellStyle={getStatusStyle}
              filter={true}
              headerName='Status'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='quantity'
              width={120}
              valueFormatter={valueFormatter}
              filter={true}
              headerName='Quantity'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='unitPrice'
              width={130}
              valueFormatter={valueFormatter}
              filter={true}
              headerName='Unit Price'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='currency'
              width={130}
              valueFormatter={valueFormatter}
              filter={true}
              headerName='Currency'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='buySell'
              width={120}
              filter={true}
              valueFormatter={valueFormatter}
              headerName='Buy/Sell'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='tradeDate'
              width={150}
              filter={'agDateColumnFilter'}
              filterParams={filterDateParams}
              valueFormatter={valueFormatter}
              headerName='Trade Date'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='settlementDate'
              width={190}
              filter={'agDateColumnFilter'}
              filterParams={filterDateParams}
              valueFormatter={valueFormatter}
              headerName='Settlement Date'
              headerClass={styles.header}
            ></AgGridColumn>
          </AgGridReact>
        </div>
      </div>
    </>
  );
};

export default Transactions;
