import React, { useRef, useState } from 'react';
import styles from './CreateAccount.module.scss';
import { Button, TextField } from '@material-ui/core';
import { Link } from '@reach/router';
import { navigate } from '@reach/router';
import { useAuth } from '../../context/AuthContext';

const CreateAccount = () => {
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const email = useRef();
  const password = useRef();
  const confirmPassword = useRef();
  const { signUp } = useAuth();

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (password.current.value !== confirmPassword.current.value) {
      return setError('Passwords have to match');
    }
    try {
      setError('');
      setLoading(true);
      await signUp(email.current.value, password.current.value);
      navigate('/daycardlist');
    } catch {
      setError('something went wrong');
    }
    setLoading(false);
  };

  return (
    <>
      <div className={styles.container}>
        {error && <div>Oh no.. {error}</div>}
        <form autoComplete='off' onSubmit={handleSubmit}>
          <TextField label='Email' type='email' inputRef={email} required />
          <TextField
            label='Password'
            type='password'
            inputRef={password}
            required
          />
          <TextField
            id='confirmPassword'
            label='Confirm Password'
            type='password'
            inputRef={confirmPassword}
            required
          />
          <div className={styles.button}>
            <Button
              disabled={loading}
              type='submit'
              variant='contained'
              type='submit'
              color='primary'
            >
              Create Account
            </Button>
          </div>
        </form>
        <span className={styles.span}>
          <Link to='/'>Back to Login </Link> |
          <Link to='/forgetPassword'> Forgot Password </Link>
        </span>
      </div>
    </>
  );
};

export default CreateAccount;
