import React from "react";
import { render } from "@testing-library/react";
import { Button } from '@material-ui/core';
import Dashboard from './Dashboard';


describe("Dashboard tests", () => {

  it("should render myBooks and table list component", () => {
    expect(render(<Dashboard/>)).toBeTruthy();
  });

  it("should render myBooks button", () => {
    expect(render(<Button/>)).toBeTruthy();
  });

});
