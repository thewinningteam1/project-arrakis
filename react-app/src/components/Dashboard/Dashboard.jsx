import React, { useState } from "react";
import BondListWithGrid from "../BondListWithGrid";
import { Button } from "@material-ui/core";
import styles from "./Dashboard.module.scss";
import Transactions from "../Transactions";

const Dashboard = ({ businessDay, maturityDate, bondType }) => {
  const [myBookSelected, setMyBookSelected] = useState(false);
  const bondFilter = bondType ? bondType : this.location.state.bondFilter;
  const [id, setId] = useState("");

  const toggleMyBooksData = (e) => {
    e.preventDefault();
    setMyBookSelected(!myBookSelected);
  };

  return (
    <div className={styles.dashboard}>
      <Button
        variant="contained"
        color="primary"
        className={styles.btn}
        onClick={toggleMyBooksData}
      >
        {myBookSelected ? "all books" : "my books"}
      </Button>
      <BondListWithGrid
        bondType={bondFilter}
        myBookSelected={myBookSelected}
        maturityDate={maturityDate}
        businessDay={businessDay}
        setId={setId}
      />
      <Transactions id={id} />
    </div>
  );
};

export default Dashboard;
