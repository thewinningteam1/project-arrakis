import { TableCell, withStyles } from '@material-ui/core';

const BondHeader = ({ heading }) => {
  const StyledTableCell = withStyles(() => ({
    head: {
      backgroundColor: 'darkblue',
      color: 'white',
      textTransform: 'capitalize',
      fontWeight: 'bolder',
    },
  }))(TableCell);

  return (
    <>
      <StyledTableCell>{heading}</StyledTableCell>
    </>
  )
}

export default BondHeader;
