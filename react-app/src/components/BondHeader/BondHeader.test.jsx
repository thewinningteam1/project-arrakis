import React from "react";
import { render } from "@testing-library/react"
import BondHeader from './BondHeader';

describe("BondHeader tests", () => {

  it("it should render bond summary table header", () => {
    expect(render(<BondHeader/>)).toBeTruthy();
  })

});
