import React from 'react';
import styles from "./BondMasterDetail.module.scss";
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import data from '../../data/allBondTrade';

const BondMasterDetail = () => {
  const rowData = data;

  const filterDateParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      var dateAsString = cellValue;
      if (dateAsString == null) return -1;
      var dateParts = dateAsString.split('/');
      var cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );

      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }

      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }

      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
  };

  const onGridReady = (params) => {
    params.api.sizeColumnsToFit();
  };

  const detailCellRendererParams = {
    detailGridOptions: {
        columnDefs: [
            { field: 'client' },
            { field: 'currency' },
            { field: 'status'},
            { field: 'quantity' },
            { field: 'unitPrice' },
            { field: 'buySell'},
            { field: 'tradeDate' },
            { field: 'settlementDate'},
            { field: 'bookId'}
        ],
        defaultColDef: { flex: 1 },
    },
    getDetailRowData: params => {
        params.successCallback(params.data.trades);
    }
  };

  const getDateFormatted = (date) => {
    let parts = date.split('/');
    return new Date(parts[2], parts[1] - 1, parts[0]);
  };

  const getMaturityDateStyle = (params) => {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    if (getDateFormatted(params.data.maturity) <= today) return { color: 'red' };
  };

  const onFirstDataRendered = (params) => {
    setTimeout(function () {
      params.api.getDisplayedRowAtIndex(0).setExpanded(true);
    }, 0);
  };

  const getRowHeight = params => {
    if (params.node && params.node.detail) {
      var offset = 80;
      var allDetailRowHeight =
        params.data.trades.length *
        params.api.getSizesForCurrentTheme().rowHeight;
      var gridSizes = params.api.getSizesForCurrentTheme();
      return allDetailRowHeight + gridSizes.headerHeight + offset;
    }
  };

  const valueFormatter = (field) => (field.value == '' ? 'N/A' : field.value);

  return (
    <>
      <div className={styles.container}>
        <div className='ag-theme-alpine' style={{ height: 700, width: 1100 }}>
          <AgGridReact
            style={{ width: '100%', height: '100%;' }}
            onGridReady={onGridReady}
            rowData={rowData}
            masterDetail={true}
            detailCellRendererParams={detailCellRendererParams}
            getRowHeight={getRowHeight}
            onFirstDataRendered={onFirstDataRendered}
          >
            <AgGridColumn
              resizable={true}
              field='isin'
              cellRenderer="agGroupCellRenderer"
              filter={true}
              valueFormatter={valueFormatter}
              headerName='ISIN'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='type'
              filter={true}
              valueFormatter={valueFormatter}
              headerName='Type'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='issuer'
              filter={true}
              valueFormatter={valueFormatter}
              headerName='Issuer'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='maturity'
              valueFormatter={valueFormatter}
              cellStyle={getMaturityDateStyle}
              filter={'agDateColumnFilter'}
              filterParams={filterDateParams}
              headerName='Maturity'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='notional'
              valueFormatter={valueFormatter}
              headerName='Notional'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='currency'
              valueFormatter={valueFormatter}
              headerName='Currency'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='coupon'
              valueFormatter={valueFormatter}
              headerName='Coupon %'
              headerClass={styles.header}
            ></AgGridColumn>
          </AgGridReact>
        </div>
      </div>
    </>
  );
};

export default BondMasterDetail;
