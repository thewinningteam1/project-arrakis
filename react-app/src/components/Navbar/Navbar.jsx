import { useState } from 'react';
import styles from './Navbar.module.scss';
import HomeIcon from '@material-ui/icons/Home';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import { Button } from '@material-ui/core';
import { navigate, Link } from '@reach/router';
import { useAuth } from '../../context/AuthContext';

const Navbar = () => {
  const [error, setError] = useState('');
  const { currentUser, logOut } = useAuth();
  const userIdStyling =
    currentUser === null ? styles.loggedOut : styles.navIcons;

  const handleLogout = async () => {
    setError('');
    try {
      await logOut();
      navigate('/');
    } catch {
      setError('Failed to log out');
    }
  };

  return (
    <div className={styles.navbar}>
      {error && <div> {error}</div>}
      <h1>Bonds</h1>
      <div className={userIdStyling}>
        <Link to='/daycardlist'>
          <HomeIcon className={styles.icon} />
        </Link>
        <PersonOutlineIcon className={styles.icon} />
        <div className={styles.userId}>
          {currentUser && currentUser.email}
          <Button onClick={handleLogout}>Log Out</Button>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
