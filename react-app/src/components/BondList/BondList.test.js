import React from 'react'
import BondList from './BondList'
import Modal from 'react-bootstrap/Modal'
import TableRow from '@material-ui/core/TableRow'
import { shallow } from 'enzyme'
import { render } from '@testing-library/react'

describe('BondList tests', () => {
  it('should render table list withh data component', () => {
    expect(render(<BondList />)).toBeTruthy()
  })
})

describe('Popup window tests', () => {
  let component;

  beforeEach(() => {
    component = shallow(<BondList />);
  })

  it('should render', () => {
    expect(component).toBeTruthy();
  })

  it('should find the Modal component', () => {
    expect(component.find(Modal)).toBeTruthy();
  })

  it('should not show the Modal component - show is disabled', () => {
    expect(component.find(Modal).prop('show')).toEqual(false);
  })

  it('should enable the Modal component', () => {
    component.find(TableRow).last().simulate('Click')
    expect(component.find(Modal).prop('show')).toEqual(true);
  })
})
