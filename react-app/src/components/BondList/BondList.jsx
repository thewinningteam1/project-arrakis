import React, { useState } from 'react';
import Bond from '../Bond/Bond';
import dummyData from '../../data/allBondData';
import BondHeader from '../BondHeader/BondHeader';
import Modal from 'react-bootstrap/Modal';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';
import styles from './BondList.module.scss';
import BondDetails from '../BondDetails';

const BondList = ({ myBookSelected }) => {
  const [bond, setBond] = useState({});
  const [display, setDisplay] = useState(false);
  const showBondDetails = (bond) => {
    handleDisplay()
    setBond(bond)
  };
  const handleClose = () => setDisplay(false);
  const handleDisplay = () => setDisplay(true);
  const tableHeading = ['id', 'isin', 'type', 'issuer', 'client', 'maturity', 'notional', 'coupon', 'status', 'bookId'];
  const headings = (Object.keys(dummyData[0])).filter((heading) => tableHeading.includes(heading));
  const myBooks = ['LENJON'];
  const myBooksData = dummyData.filter((data) => myBooks.includes(data.bookId));

  return (
    <>
      <div>
        <Modal onHide={handleClose} show={display}>
            <BondDetails bond={bond} handleClose={handleClose} />
        </Modal>
      </div>
      <TableContainer className={styles.bondtable}>
        <Table>
          <TableHead>
            <TableRow hover>
              {headings.map((heading) => (
                <BondHeader heading={heading} />
              ))}
            </TableRow>
          </TableHead>
          {myBookSelected
            ? myBooksData.map((bond) => (
                <TableRow hover onClick={() => showBondDetails(bond)}>
                  <Bond bond={bond} />
                </TableRow>
              ))
            : dummyData.map((bond) => (
                <TableRow hover onClick={() => showBondDetails(bond)}>
                  <Bond bond={bond} />
                </TableRow>
              ))}
        </Table>
      </TableContainer>
    </>
  )
}

export default BondList
