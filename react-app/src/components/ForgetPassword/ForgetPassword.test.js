
import React from 'react';
import { shallow } from "enzyme";
import ForgetPassword from './ForgetPassword';
import { TextField } from '@material-ui/core';

let wrapper;
  describe('<ForgetPassword />', () => {
    beforeEach(() => {
    wrapper = shallow(<ForgetPassword />);
    })
    it('should render component', () => {
        expect(wrapper).toBeTruthy;
    });
    it('should render the three TextField components', () => {
      expect(wrapper.find(TextField).length).toEqual(3);
    });
    it('First TextField should be the username', () => {
      expect(wrapper.find(TextField).first().prop('label')).toBe("props.UserName");
    });
    it('Third TextField should be the Confirm Password', () => {
      expect(wrapper.find(TextField).last().prop('label')).toBe("Confirm Password");
    });
    it('should render a button with Submit', () => {
      expect(wrapper.find(".button").first().text().trim()).toBe("Submit");
    });
    it('should render a button with Clear', () => {
      expect(wrapper.find(".button").last().text().trim()).toBe("Clear");
    });
    it('New Password and Confirm password fields are empty', () => {
      wrapper.find(".button").last().simulate("Click");
      expect(wrapper.findByLabelText("New Password")).toHaveValue("");
      expect(wrapper.findByLabelText("Confirm Password")).toHaveValue("");
    })
});
