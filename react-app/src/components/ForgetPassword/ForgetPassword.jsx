import React,{useRef } from "react";
import styles from "./ForgetPassword.module.scss";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const ForgetPassword = (props) => {

const textRefNew = useRef();
const textRefConfirm = useRef();

const handleSubmit = () => {
  var newPassword = textRefNew.current.value;
  var confirmPassword = textRefConfirm.current.value;
  const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  if (newPassword === confirmPassword) {
    if (newPassword.length <8 || (!strongRegex.test(newPassword))) {
      alert("Password has to be at least 8 characters, one Uppercase, one lowercase, one number, one special character");
      return;
    } else {
      alert("Password updated");
    }
  } else {
    alert("Password Doesn't Match");
  }
};

const handleClear = () => {
  textRefNew.current.value="";
  textRefConfirm.current.value="";
  return;
};

return (
  <>
    <form className={styles.form} noValidate autoComplete="off">
      <div className={styles.text}>
        <TextField id="filled-basic" label="Enter user email" type="email" />
        <TextField id="standard-basic"
          inputRef={textRefNew}
          label="New Password"
          type="password"
          required
        />
        <TextField
          inputRef={textRefConfirm}
          id="outlined-basic"
          label="Confirm Password"
          type="password"
          required
        />
      </div>
      <Button variant="contained" className={styles.submit}  color="primary" onClick={handleSubmit}>
        Submit
      </Button>
      <Button variant="contained" className={styles.clear}  color="primary" onClick={handleClear}>
        Clear
      </Button>
    </form>
  </>
  );
};

export default ForgetPassword;
