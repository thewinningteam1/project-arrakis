import React from 'react';
import TableCell from '@material-ui/core/TableCell';

const Bond = ({ bond }) => {
  return (
    <>
      <TableCell>{bond.id}</TableCell>
      <TableCell>{bond.isin}</TableCell>
      <TableCell>{bond.type}</TableCell>
      <TableCell>{bond.issuer}</TableCell>
      <TableCell>{bond.client}</TableCell>
      <TableCell>{bond.maturity}</TableCell>
      <TableCell>{bond.notional + " " + bond.currency}</TableCell>
      <TableCell>{bond.coupon}%</TableCell>
      <TableCell>{bond.status}</TableCell>
      <TableCell>{bond.bookId}</TableCell>
    </>
  );
};

export default Bond;
