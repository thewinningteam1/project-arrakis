import React from "react";
import { render } from "@testing-library/react";
import BondListWithGrid from "./BondListWithGrid";
import { shallow } from "enzyme";
import {AgGridReact} from 'ag-grid-react';
import testData from "../../data/allBondData";

let component;
let agGridReact = null;

const ensureGridApiHasBeenSet = (component) => {
  return new Promise(function (resolve) {
    (function waitForGridReady() {
      if (component.gridApi) {
        resolve(component);
        return;
      }
      setTimeout(waitForGridReady, 100);
    })();
  });
};

const setRowData = (component, rowData) => {
  return new Promise(function (resolve) {
    component.setState({ rowData }, () => {
      component.update();
      resolve();
    });
  })
}

beforeEach(() => {
  component = shallow(<BondListWithGrid/>)
  agGridReact = component.find(AgGridReact);
  ensureGridApiHasBeenSet(component)
    .then(() => setRowData(component, testData))
    .then(() => done());
});

describe("BondListWithGrid", () => {
  it("should render", () => {
    expect(render(<BondListWithGrid />)).toBeTruthy();
  });

  it("should render the aggrid", () => {
    expect(component.find(AgGridReact)).toBeTruthy();
  });
});
