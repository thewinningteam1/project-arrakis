import React, { useState, useEffect } from 'react';
import styles from './BondListWithGrid.module.scss';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { useAuth } from '../../context/AuthContext';
import { findSecuritiesByUser, findAllSecuritiesByMaturityDate, findAllSecurities  } from '../../services/BondServices';

const BondListWithGrid = ({
  myBookSelected,
  bondType,
  maturityDate,
  setId,
}) => {

  let rowData = '';
  const [securityData, setSecurityData] = useState([]);
  const {currentUser} = useAuth();

  useEffect(() => {
    if (myBookSelected) {
      findSecuritiesByUser(currentUser.email)
        .then(({data}) => setSecurityData(data.content));
    } else if (maturityDate !== null && maturityDate.trim() !== '') {
      findAllSecuritiesByMaturityDate(maturityDate)
        .then(({data}) => setSecurityData(data));
    } else {
      findAllSecurities()
          .then(({data}) => setSecurityData(data));
    }
  }, [myBookSelected]);

  rowData = securityData;

  if (bondType !== null && bondType.trim() !== '') {
    rowData = securityData.filter(
        (bond) => bond.type === bondType);
  }

  const filterDateParams = {
    comparator: function (filterLocalDateAtMidnight, cellValue) {
      const dateAsString = cellValue;
      if (dateAsString === null) return -1;
      const dateParts = dateAsString.split('/');
      const cellDate = new Date(
        Number(dateParts[2]),
        Number(dateParts[1]) - 1,
        Number(dateParts[0])
      );

      if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
        return 0;
      }

      if (cellDate < filterLocalDateAtMidnight) {
        return -1;
      }

      if (cellDate > filterLocalDateAtMidnight) {
        return 1;
      }
    },
    browserDatePicker: true,
  };

  const onGridReady = (params) => {
    params.api.sizeColumnsToFit();
  };

  const getStatusStyle = (params) => {
    return params.data.status === 'active' ? { color: 'green' } : { color: 'red'};
  };

  const getDateFormatted = (date) => {
    const parts = date.split('/');
    return new Date(parts[2], parts[1] - 1, parts[0]);
  };

  const getMaturityDateStyle = (params) => {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    if (getDateFormatted(params.data.maturityDate) <= today) return { color: 'red' };
  };

  const onRowClicked = (event) => {
    setId(event.data.id);
  };

  const valueFormatter = (field) => (field.value === '' ? 'N/A' : field.value);

  return (
    <>
      <div className={styles.container}>
        <div className='ag-theme-alpine' style={{ height: 300, width: 1300 }}>
          <AgGridReact
            pagination={true}
            paginationPageSize={10}
            style={{ width: '100%', height: '100%;' }}
            onRowClicked={onRowClicked}
            onGridReady={onGridReady}
            rowData={rowData}
          >
            <AgGridColumn
              resizable={true}
              field='isin'
              filter={true}
              valueFormatter={valueFormatter}
              headerName='ISIN'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='type'
              filter={true}
              valueFormatter={valueFormatter}
              headerName='Type'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='issuerName'
              filter={true}
              valueFormatter={valueFormatter}
              headerName='Issuer'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='maturityDate'
              valueFormatter={valueFormatter}
              cellStyle={getMaturityDateStyle}
              filter={'agDateColumnFilter'}
              filterParams={filterDateParams}
              headerName='Maturity'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='faceValue'
              valueFormatter={valueFormatter}
              headerName='Face Value'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='currency'
              valueFormatter={valueFormatter}
              headerName='Currency'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='coupon'
              valueFormatter={valueFormatter}
              headerName='Coupon %'
              headerClass={styles.header}
            ></AgGridColumn>
            <AgGridColumn
              resizable={true}
              field='status'
              valueFormatter={valueFormatter}
              cellStyle={getStatusStyle}
              filter={true}
              headerName='Status'
              headerClass={styles.header}
            ></AgGridColumn>
          </AgGridReact>
        </div>
      </div>
    </>
  );
};

export default BondListWithGrid;
