import React, { useRef, useState } from 'react';
import styles from './Login.module.scss';
import { Button, TextField } from '@material-ui/core';
import { navigate } from '@reach/router';
import { Link } from '@reach/router';
import { useAuth } from '../../context/AuthContext';

const Login = () => {
  const email = useRef();
  const password = useRef();
  const { logIn } = useAuth();
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setError('');
      setLoading(true);
      await logIn(email.current.value, password.current.value);
      navigate('/daycardlist');
    } catch {
      setError('Failed to log in');
    }
    setLoading(false);
  };

  return (
    <div className={styles.container}>
      {error && <div> {error}</div>}
      <form autoComplete='off' className={styles.form}>
        <TextField
          id='email'
          label='Username / Email'
          type='email'
          required
          inputRef={email}
        />

        <TextField
          id='password'
          label='Password'
          type='password'
          required
          inputRef={password}
        />

        <div className={styles.button}>
          <Button
            onClick={handleSubmit}
            variant='contained'
            type='submit'
            color='primary'
          >
            Login
          </Button>
        </div>
        <span className={styles.span}>
          <Link to='createAccount'>Create Account </Link> |
          <Link to='forgetPassword'> Forgot Password </Link>
        </span>
      </form>
    </div>
  );
};

export default Login;
