import React from "react";
import Login from "./Login";
import { shallow } from "enzyme";
import { Button, TextField } from '@material-ui/core';

describe("Login tests", () => {
  let component;
  beforeEach(() => {
    component = shallow(<Login/>);
  });

  it("should render Login component", () => {
    expect(component).toBeTruthy();
  });

  it("should render header with text Deutsche Bonds", () => {
    expect(component.find('h3').text().trim()).toBe("Deutsche Bonds");
  });

  it("should render header with image Deutsche logo", () => {
    expect(component.find('img').prop('src')).toBe("dbLogo.png");
  });

  it("should render text field with Username as placeholder", () => {
    expect(component.find(TextField).first().prop('label')).toBe("Username");
  });

  it("should render text field with Password as Password", () => {
    expect(component.find(TextField).last().prop('label')).toBe("Password");
  });

  it("should render button with text Login", () => {
    expect(component.find(Button).text().trim()).toBe("Login");
  });

  it("should render Forgot password as a text", () => {
    expect(component.find('.span').text().trim()).toBe("Forgot Password");
  });
});
