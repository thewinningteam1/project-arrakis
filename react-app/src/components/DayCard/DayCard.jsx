import React from 'react';
import DayCardHeader from '../DayCardHeader';
import styles from './DayCard.module.scss';
import BondSummary from '../BondSummary';



const DayCard = (props) => {

  const {businessDay, maturityDate,setMaturityDate,bondType,setBondType} = props;

  return (
    <div className={styles.dayCard}>
      <DayCardHeader businessDay = {businessDay} setMaturityDate={setMaturityDate} maturityDate={maturityDate} bondType={bondType} setBondType={setBondType}/>
      <BondSummary  businessDay = {businessDay} maturityDate={maturityDate} setMaturityDate={setMaturityDate} bondType={bondType} setBondType={setBondType}/>
    </div>
  );
};

export default DayCard;
