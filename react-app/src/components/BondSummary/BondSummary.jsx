import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { navigate } from '@reach/router';
import { findAllSecurities } from "../../services/BondServices";

const BondSummary = (props) => {
  const {
    businessDay,
    setMaturityDate,
    setBondType,
  } = props;

  const [securities, setSecurities] = useState([]);

  useEffect(() => {
    findAllSecurities()
          .then(({data}) => {
            setSecurities(data);
          });
  }, []);

  var cardStyle = {
    display: 'block',
    width: '340',
    height: '22vw',
    padding: '5px',
    margin: '1px',
  };

  const useStyles = makeStyles({
    root: {
      maxWidth: 340,
    },
    action: {},
  });

  const classes = useStyles();
  const typeOfBond = [
    'Sovereign',
    'Corporate',
    'Municipal',
    'Government',
    'Supranational',
    'Agency',
  ];

  const filteredDataOnMaturityDate = securities.filter((data) =>
    data.maturityDate.includes(businessDay.date)
  );

  const getBondStats = (bondType) => {
    const recordCount = filteredDataOnMaturityDate.filter((data) =>
      data.type.includes(bondType)
    ).length;
    let isDisplay = false;
    if (recordCount > 0) isDisplay = true;
    return { recordCount, isDisplay };
  };

  const setCardContent = (bondType) => (
    <CardActions className={classes.action}>
      <label htmlFor=''></label>
      <Button
        size='small'
        color='primary'
        onClick={() => handleLinkBond(bondType)}
      >
        {bondType}
      </Button>
      <Button
        size='small'
        color='primary'
        onClick={() => handleLinkBond(bondType)}
      >
        {getBondStats(bondType).recordCount}
      </Button>
    </CardActions>
  );

  const handleLinkBond = (bondType) => {
    setMaturityDate(businessDay.date);
    setBondType(bondType);
    navigate('/dashboard', { state: { bondFilter: bondType } });
  };

  return (
    <>
      <Card className={classes.root} style={cardStyle} boxShadow={2}>
        {filteredDataOnMaturityDate.length > 0 ? (
          typeOfBond.map(
            (bondType) =>
              getBondStats(bondType).isDisplay && setCardContent(bondType)
          )
        ) : (
          <CardActions>
            {' '}
            <Button size='small' color='primary'>
              No Bonds Expiring
            </Button>{' '}
          </CardActions>
        )}
      </Card>
    </>
  );
};

export default BondSummary;
