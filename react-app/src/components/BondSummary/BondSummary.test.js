import React from "react";
import { render } from "@testing-library/react";
import BondSummary from "./BondSummary";
import { shallow } from "enzyme";

describe("BondSummary tests", () => {
  it("should render", () => {
    expect(render(<BondSummary />)).toBeTruthy();
  });
});


describe('Props are available in the Bond Summary component', () => {
  let component

  beforeEach(() => {
    component = shallow(<BondSummary/>);
  })

  it('should render', () => {
    expect(component).toBeTruthy();
  })


  it('to check the maturity date props are available', () => {
    expect(component.prop('maturityDate')).toBeValid;
  })


  it('to check the bond type props are available', () => {
    expect(component.prop('bondType')).toBeValid;
  })


  it('to check the colour props are available', () => {
    expect(component.prop('colour')).toBeValid;
  })

  it('to check the Client section', () => {
    expect(component.find('client')).toBeTruthy;
  })

})
