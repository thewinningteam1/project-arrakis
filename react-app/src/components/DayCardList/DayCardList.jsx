import styles from './DayCardList.module.scss';
import DayCard from '../DayCard';

const DayCardList = (props) => {

  const { setMaturityDate,bondType,setBondType, setBusinessDay } = props;
  const moment = require('moment-business-days');
  const calendarToday = new Date();
  const yesterday = moment(calendarToday).subtract(1, 'days').format("DD/MM/yyyy");
  const today = moment(yesterday, 'DD/MM/yyyy').nextBusinessDay().format('DD/MM/yyyy');
  const todayPlusOne =  moment(today, 'DD/MM/yyyy').nextBusinessDay().format('DD/MM/yyyy');
  const todayPlusTwo  =  moment(todayPlusOne, 'DD/MM/yyyy').nextBusinessDay().format('DD/MM/yyyy');
  const todayPlusThree  =  moment(todayPlusTwo, 'DD/MM/yyyy').nextBusinessDay().format('DD/MM/yyyy');
  const todayPlusFour  =  moment(todayPlusThree, 'DD/MM/yyyy').nextBusinessDay().format('DD/MM/yyyy');
  const displayToday = moment(yesterday, 'dddd Do MMM').nextBusinessDay().format('dddd Do MMM');
  const displayTodayPlusOne =  moment(today, 'dddd Do MMM').nextBusinessDay().format('dddd Do MMM');
  const displayTodayPlusTwo  =  moment(todayPlusOne, 'dddd Do MMM').nextBusinessDay().format('dddd Do MMM');
  const displayTodayPlusThree  =  moment(todayPlusTwo, 'dddd Do MMM').nextBusinessDay().format('dddd Do MMM');
  const displayTodayPlusFour  =  moment(todayPlusThree, 'dddd Do MMM').nextBusinessDay().format('dddd Do MMM');
  const businessDaysArray = [
    {date:today, displayDate:displayToday, urgency: 'high'},
    {date:todayPlusOne,  displayDate:displayTodayPlusOne, urgency: 'medium'},
    {date:todayPlusTwo,  displayDate:displayTodayPlusTwo,  urgency: 'medium'},
    {date:todayPlusThree,  displayDate:displayTodayPlusThree, urgency: 'low'},
    {date:todayPlusFour,  displayDate:displayTodayPlusFour,  urgency: 'low'}
  ];

  return (
    <div className = {styles.DayCardList}>
      {
        businessDaysArray.map(businessDay =>
          <DayCard
            businessDay={businessDay}
            setBusinessDay={setBusinessDay}
            setMaturityDate={setMaturityDate}
            maturityDate={businessDay.date}
            bondType={bondType}
            setBondType={setBondType}
          />
        )
      }
    </div>
  );
};

export default DayCardList;
