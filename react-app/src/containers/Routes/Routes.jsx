import React from "react";
import { Router } from "@reach/router";
import Login from "../../components/Login";
import ForgetPassword from "../../components/ForgetPassword";
import Dashboard from "../../components/Dashboard";
import DayCardList from "../../components/DayCardList";
import CreateAccount from "../../components/CreateAccount";
import BondMasterDetail from "../../components/BondMasterDetail";

const Routes = ({
  businessDay,
  setBusinessDay,
  maturityDate,
  setMaturityDate,
  bondType,
  setBondType,
}) => {
  return (
    <>
      <Router>
        <Login path="/" />
        <CreateAccount path="createAccount" />
        <ForgetPassword path="forgetPassword" />
        <Dashboard
          path="dashboard"
          businessDay={businessDay}
          maturityDate={maturityDate}
          bondType={bondType}
        />
        <DayCardList
          path="daycardlist"
          setBusinessDay={setBusinessDay}
          setMaturityDate={setMaturityDate}
          bondType={bondType}
          setBondType={setBondType}
        />
        <BondMasterDetail path="group" />
      </Router>
    </>
  );
};

export default Routes;
