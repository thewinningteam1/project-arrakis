import { hostNameUrl } from "../config/api";
import axios from "axios";

export const findTradesForSecurity = (id) => {
  return axios.get(`${hostNameUrl}/securities/${id}/trades`);
};

export const findAllSecuritiesInPages = () => {
    return axios.get(`${hostNameUrl}/securities/pages`);
};

export const findAllSecurities = () => {
  return axios.get(`${hostNameUrl}/securities`);
};

export const findAllSecuritiesByMaturityDate = (startDate) => {
  return axios.get(`${hostNameUrl}/securities?startDate=${startDate}`);
};

export const findAllSecuritiesByRange = (startDate, endDate) => {
  return axios.get(`${hostNameUrl}/securities?startDate=${startDate}&endDate=${endDate}`);
};

export const findSecuritiesByUser = (userEmail) => {
  return axios.get(`${hostNameUrl}/securities/users?userEmail=${userEmail}`);
};
