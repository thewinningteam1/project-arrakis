from flask import Flask, jsonify
from flask_restx import Api
from flask_cors import CORS
from app.extensions import mysql
from app.Dogs.controller import api as dogs_api
import os


def create_app():
    # Create a Flask application
    app = Flask(__name__)

    # Allow cross-origin resource sharing to let the front end access the backend
    CORS(app)

    # The flask_restx library helps us to create documentation at the /swagger-ui/ endpoint
    api = Api(app, title="Dogs API", version="0.1", doc="/swagger-ui/")

    # Connect your database
    if os.getenv("GAE_ENV", "").startswith("standard"):
        # For production in GAE Standard. Get from environment variables from app.yaml.
        # Remember - storing secrets in plaintext is potentially unsafe. Consider using
        # something like https://cloud.google.com/secret-manager/docs/overview to help keep
        # secrets secret.
        app.config["MYSQL_DATABASE_USER"] = os.environ["DB_USER"]
        app.config["MYSQL_DATABASE_PASSWORD"] = os.environ["DB_PASS"]
        app.config["MYSQL_DATABASE_DB"] = os.environ["DB_NAME"]
        db_socket_dir = os.environ.get("DB_SOCKET_DIR", "/cloudsql")
        cloud_sql_connection_name = os.environ["CLOUD_SQL_CONNECTION_NAME"]
        app.config["MYSQL_DATABASE_SOCKET"] = "{}/{}".format(
            db_socket_dir, cloud_sql_connection_name
        )
    else:
        # For local execution
        app.config["MYSQL_DATABASE_HOST"] = "localhost"
        app.config["MYSQL_DATABASE_PORT"] = 3306
        app.config["MYSQL_DATABASE_USER"] = "root"
        app.config["MYSQL_DATABASE_PASSWORD"] = "Password123"
        app.config["MYSQL_DATABASE_DB"] = "pets"

    # Start the connector (found in extensions.py)
    mysql.init_app(app)

    # We add the Dogs module
    api.add_namespace(dogs_api, path="/dogs")

    @app.route("/health")
    def health():
        return jsonify("healthy")

    return app
