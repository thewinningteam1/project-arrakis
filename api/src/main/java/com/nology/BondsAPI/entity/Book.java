package com.nology.BondsAPI.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "books")
    private List<User> users;

    @OneToMany(mappedBy = "book")
    @JsonManagedReference
    private List<Trade> trades;

    public Book(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Book() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Trade> getTrades() {
        return trades;
    }

    public void setTrades(List<Trade> trades) {
        this.trades = trades;
    }

    public Integer getId() {
        return id;
    }
}
