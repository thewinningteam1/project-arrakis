package com.nology.BondsAPI.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "trades")
public class Trade {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    //    @ManyToOne(fetch = FetchType.LAZY)  // may be changed to EAGER
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "security_id", nullable = false)
    @JsonBackReference
    private Security security;

    @ManyToOne(fetch = FetchType.LAZY)
//    @ManyToOne
    @JoinColumn(name = "book_id", nullable = false)
    @JsonBackReference
    private Book book;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "counterparty_id", nullable = false)
    @JsonBackReference
    private Counterparty counterparty;

    private String currency;

    private String status;

    private int quantity;

    private double unitPrice;

    private String buySell;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate tradeDate;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate settlementDate;

    @Transient
    private String counterpartyName;

    @Transient
    private String bookName;

    @Transient
    private int securityId;

    @Transient
    private String isin;


    public Trade() {

    }

    public Trade(Integer id, Security security, Book book, Counterparty counterparty, String currency, String status, int quantity, double unitPrice, String buySell, LocalDate tradeDate, LocalDate settlementDate) {
        this.id = id;
        this.security = security;
        this.book = book;
        this.counterparty = counterparty;
        this.currency = currency;
        this.status = status;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.buySell = buySell;
        this.tradeDate = tradeDate;
        this.settlementDate = settlementDate;
    }


    public Trade(int id, String currency, String status, int quantity, double unitPrice, String buySell, LocalDate tradeDate, LocalDate settlementDate) {
        this.id = id;
        this.currency = currency;
        this.status = status;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.buySell = buySell;
        this.tradeDate = tradeDate;
        this.settlementDate = settlementDate;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Counterparty getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(Counterparty counterparty) {
        this.counterparty = counterparty;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getBuySell() {
        return buySell;
    }

    public void setBuySell(String buySell) {
        this.buySell = buySell;
    }

    public LocalDate getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(LocalDate tradeDate) {
        this.tradeDate = tradeDate;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(LocalDate settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getSecurityId() {
        return securityId;
    }

    public void setSecurityId(int securityId) {
        this.securityId = securityId;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }
}
