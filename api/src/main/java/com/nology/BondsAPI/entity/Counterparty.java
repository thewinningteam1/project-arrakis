package com.nology.BondsAPI.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "counterparty")
public class Counterparty {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    private String name;

    public Counterparty() {

    }

    public Counterparty(Integer id, String name) {
        this.id = id;
        this.name = name;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }


}
