package com.nology.BondsAPI.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static javax.persistence.GenerationType.AUTO;

@Entity

public class Security {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    private String isin;

    private String cusip;

    @NotBlank(message = "Issuer is mandatory")
    private String issuerName;

    //    @NotBlank(message = "Maturity Date is mandatory")
    @NotNull()
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate maturityDate;

    @NotBlank(message = "Bond Type is mandatory")
    private String type;

    @NotBlank(message = "Coupon is mandatory")
    private double coupon;

    @NotBlank(message = "Face Value is mandatory")
    private double faceValue;

    @NotBlank(message = "Status is mandatory")
    private String status;

    @NotBlank(message = "Currency is mandatory")
    private String currency;

    @OneToMany(mappedBy = "security")
//    @JsonManagedReference
    @JsonIgnore
    private List<Trade> trades;

    public Security() {
    }

    public Security(String isin, String cusip, String issuerName, LocalDate maturityDate, String type, double coupon, double faceValue, String status, String currency) {
        this.isin = isin;
        this.cusip = cusip;
        this.issuerName = issuerName;
        this.maturityDate = maturityDate;
        this.type = type;
        this.coupon = coupon;
        this.faceValue = faceValue;
        this.status = status;
        this.currency = currency;
    }

    public Security(Integer id, String isin, String cusip, @NotBlank(message = "Issuer is mandatory") String issuerName, @NotNull() LocalDate maturityDate, @NotBlank(message = "Bond Type is mandatory") String type, @NotBlank(message = "Coupon is mandatory") double coupon, @NotBlank(message = "Face Value is mandatory") double faceValue, @NotBlank(message = "Status is mandatory") String status, @NotBlank(message = "Currency is mandatory") String currency) {
        this.id = id;
        this.isin = isin;
        this.cusip = cusip;
        this.issuerName = issuerName;
        this.maturityDate = maturityDate;
        this.type = type;
        this.coupon = coupon;
        this.faceValue = faceValue;
        this.status = status;
        this.currency = currency;
    }

    public Integer getId() {
        return id;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public LocalDate getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(LocalDate maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCusip() {
        return cusip;
    }

    public void setCusip(String cusip) {
        this.cusip = cusip;
    }

    public double getCoupon() {
        return coupon;
    }

    public void setCoupon(double coupon) {
        this.coupon = coupon;
    }

    public double getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(double faceValue) {
        this.faceValue = faceValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<Trade> getTrades() {
//        return trades;
        return trades.stream().map(trade -> {
            trade.setSecurityId(trade.getSecurity().getId());
            trade.setBookName(trade.getBook().getName());
            trade.setCounterpartyName(trade.getCounterparty().getName());
            trade.setIsin(trade.getSecurity().getIsin());

            return trade;
        }).collect(Collectors.toList());
    }

    public void setTrades(List<Trade> trades) {
        this.trades = trades;
    }
}
