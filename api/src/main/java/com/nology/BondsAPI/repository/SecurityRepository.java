package com.nology.BondsAPI.repository;

import com.nology.BondsAPI.entity.Security;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface SecurityRepository extends JpaRepository<Security, Integer> {

    List<Security> findByMaturityDate(LocalDate startDate);
}
