package com.nology.BondsAPI.repository;

import com.nology.BondsAPI.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String userEmail);
}
