package com.nology.BondsAPI.repository;

import com.nology.BondsAPI.entity.Book;
import com.nology.BondsAPI.entity.Trade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TradeRepository extends JpaRepository<Trade, Integer> {
    Page<Trade> findBySecurityId(int securityId, Pageable elementForPageRequest);

    List<Trade> findByBook(Book book);
}
