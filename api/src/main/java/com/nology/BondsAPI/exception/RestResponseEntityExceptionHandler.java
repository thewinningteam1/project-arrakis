package com.nology.BondsAPI.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {
    @ExceptionHandler(SecurityNotFoundException.class)
    public ResponseEntity handleNoSuchElementFoundException(
            SecurityNotFoundException exception
    ) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .header("Access-Control-Allow-Origin", "*")
                .body(exception);
    }

    @ExceptionHandler(TradeNotFoundException.class)
    public ResponseEntity handleNoSuchElementFoundException(
            TradeNotFoundException exception
    ) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .header("Access-Control-Allow-Origin", "*")
                .body(exception);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity handleNoSuchElementFoundException(
            UserNotFoundException exception
    ) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .header("Access-Control-Allow-Origin", "*")
                .body(exception);
    }

    @ExceptionHandler(BookNotFoundException.class)
    public ResponseEntity handleNoSuchElementFoundException(
            BookNotFoundException exception
    ) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .header("Access-Control-Allow-Origin", "*")
                .body(exception);
    }


    //    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Map<String, String>> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex) {

        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header("Access-Control-Allow-Origin", "*")
                .body(errors);
    }
}
