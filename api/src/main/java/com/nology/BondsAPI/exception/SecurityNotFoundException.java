package com.nology.BondsAPI.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Security not found")
public class SecurityNotFoundException extends RuntimeException {
    public SecurityNotFoundException(String message) {
        super(message);
    }
}
