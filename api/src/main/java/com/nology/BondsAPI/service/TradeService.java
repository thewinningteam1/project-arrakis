package com.nology.BondsAPI.service;

import com.nology.BondsAPI.entity.Trade;
import com.nology.BondsAPI.exception.TradeNotFoundException;
import com.nology.BondsAPI.repository.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class TradeService {

    @Autowired
    private TradeRepository tradeRepo;

    public Trade createTrade(Trade newTrade) {
        return tradeRepo.save(newTrade);
    }

    public Page<Trade> findTradesForSecurity(int securityId, int page, int size) {
        Pageable elementForPageRequest = PageRequest.of(page, size);
        return tradeRepo.findBySecurityId(securityId, elementForPageRequest);
    }

    public Trade findTrade(int tradeId) {
        try {
            return tradeRepo.findById(tradeId).get();
        } catch (NoSuchElementException exception) {
            throw new TradeNotFoundException("Trade not found with id " + tradeId);
        }
    }

    public Trade updateTrade(int tradeId, Trade updatedTrade) {
        Trade trade = findTrade(tradeId);
        trade.setBuySell(updatedTrade.getBuySell());
        trade.setCounterparty(updatedTrade.getCounterparty());
        trade.setCurrency(updatedTrade.getCurrency());
        trade.setStatus(updatedTrade.getStatus());
        trade.setQuantity(updatedTrade.getQuantity());
        trade.setUnitPrice(updatedTrade.getUnitPrice());
        trade.setTradeDate(updatedTrade.getTradeDate());
        trade.setSettlementDate(updatedTrade.getSettlementDate());

        return trade;
    }
}
