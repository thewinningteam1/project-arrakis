package com.nology.BondsAPI.service;

import com.nology.BondsAPI.entity.Book;
import com.nology.BondsAPI.entity.Security;
import com.nology.BondsAPI.entity.Trade;
import com.nology.BondsAPI.entity.User;
import com.nology.BondsAPI.exception.BookNotFoundException;
import com.nology.BondsAPI.exception.SecurityNotFoundException;
import com.nology.BondsAPI.exception.UserNotFoundException;
import com.nology.BondsAPI.repository.BookRepository;
import com.nology.BondsAPI.repository.SecurityRepository;
import com.nology.BondsAPI.repository.TradeRepository;
import com.nology.BondsAPI.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SecurityService {

    @Autowired
    private SecurityRepository securityRepo;

    @Autowired
    private TradeRepository tradeRepo;

    @Autowired
    private BookRepository bookRepo;

    @Autowired
    private UserRepository userRepo;


    public Page<Security> findAllSecurities(int page, int size) {
        Pageable elementForPageRequest = PageRequest.of(page, size);
        return securityRepo.findAll(elementForPageRequest);
    }

    public List<Security> findAllSecurities() {
        return securityRepo.findAll();
    }

    public Security findSecurity(Integer id) {
        try {
            return securityRepo.findById(id).get();
        } catch (NoSuchElementException ex) {
            throw new SecurityNotFoundException("Security not found with id " + id);
        }
    }

    public List<Trade> findTradesForSecurity(int id) {
        try {
            Security security = securityRepo.findById(id).orElse(null);
            if (security == null) throw new NoSuchElementException();
            return security.getTrades();
        } catch (NoSuchElementException exception) {
            throw new SecurityNotFoundException("Security not found with id " + id);
        }
    }

    public Security createSecurity(Security security) {
        return securityRepo.save(security);
    }

    public Security updateSecurity(Integer id, Security security) {
        try {
            Security updatedSecurity = findSecurity(id);
            updatedSecurity.setIsin(security.getIsin());
            updatedSecurity.setIssuerName(security.getIssuerName());
            updatedSecurity.setMaturityDate(security.getMaturityDate());
            updatedSecurity.setType(security.getType());

            return securityRepo.save(updatedSecurity);
        } catch (NoSuchElementException ex) {
            throw new SecurityNotFoundException("Security not found with id " + id);
        }
    }

    public void deleteSecurity(Integer id) {
        try {
            securityRepo.deleteById(id);
        } catch (NoSuchElementException ex) {
            throw new SecurityNotFoundException("Security not found with id " + id);
        }
    }

    public List<Security> getSecurityByDateRange(LocalDate startDate, LocalDate endDate) {
        try {
            return securityRepo.findAll().stream().filter(security -> security.getMaturityDate()
                    .isAfter(startDate.minusDays(1)) && security.getMaturityDate().isBefore(endDate.plusDays(1)))
                    .collect(Collectors.toList());
        } catch (NoSuchElementException ex) {
            throw new SecurityNotFoundException("No securities available between " + startDate + " and " + endDate);
        }
    }

    public List<Security> getSecurityByDate(LocalDate startDate) {
        try {
            return securityRepo.findByMaturityDate(startDate);
        } catch (NoSuchElementException ex) {
            throw new SecurityNotFoundException("No securities available for this date: " + startDate);
        }
    }

    public Page<Security> findSecuritiesByUser(String userEmail, int page, int size) {
        try {
            User tempUser = userRepo.findByEmail(userEmail);
            if (tempUser == null)
                throw new UserNotFoundException("No user found for user id " + userEmail);

            List<Book> books = bookRepo.findByUsers_Id(tempUser.getId());
            if (books != null && books.size() == 0)
                throw new BookNotFoundException("No books found for user id " + userEmail);

            Pageable elementForPageRequest = PageRequest.of(page, size);
            List<Security> securities = new ArrayList<>(books.stream()
                    .flatMap(book -> tradeRepo.findByBook(book).stream().map(Trade::getSecurity))
                    .sorted(Comparator.comparing(Security::getMaturityDate))
                    .collect(Collectors.toCollection(LinkedHashSet::new)));

            if (securities.size() == 0)
                throw new SecurityNotFoundException("No securities found for user id " + userEmail);

            int start = (int) elementForPageRequest.getOffset();
            int end = Math.min((start + elementForPageRequest.getPageSize()), securities.size());
            return new PageImpl<Security>(securities.subList(start, end), elementForPageRequest, securities.size());

        } catch (NoSuchElementException ex) {
            throw new UserNotFoundException("No user found with id " + userEmail);
        }
    }
}
