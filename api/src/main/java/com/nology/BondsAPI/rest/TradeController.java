package com.nology.BondsAPI.rest;

import com.nology.BondsAPI.entity.Trade;
import com.nology.BondsAPI.service.TradeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trades")
public class TradeController {

    @Autowired
    private TradeService service;

    @ApiOperation(value = "Creates a new trade", notes = "createTrade", nickname = "createTrade")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Trade successfully created"),
            @ApiResponse(code = 404, message = "Failed to create trade")})
    @PostMapping
    public ResponseEntity createTrade(@RequestBody Trade newTrade) {
        Trade createdTrade = service.createTrade(newTrade);
        return ResponseEntity.status(HttpStatus.OK).body(createdTrade);
    }

    @ApiOperation(value = "Returns trades for a given security across a number of pages", notes = "findTradesForSecurity", nickname = "findTradesForSecurity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Trades successfully found"),
            @ApiResponse(code = 404, message = "Trades not found")})
    @GetMapping
    public ResponseEntity findTradesForSecurity(@RequestParam int securityId,
                                                @RequestParam(defaultValue = "0") int page,
                                                @RequestParam(defaultValue = "10") int size) {
        Page<Trade> trades = service.findTradesForSecurity(securityId, page, size);
        return ResponseEntity.status(HttpStatus.OK).body(trades);
    }

    @ApiOperation(value = "Returns a trade based on a given trade ID", notes = "findTrade", nickname = "findTrade")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Trade successfully found"),
            @ApiResponse(code = 404, message = "Trade not found")})
    @GetMapping("/{tradeId}")
    public ResponseEntity findTrade(@PathVariable int tradeId) {
        Trade trade = service.findTrade(tradeId);
        return ResponseEntity.status(HttpStatus.OK).body(trade);
    }

    @ApiOperation(value = "Updates a given trade", notes = "UpdateTrade", nickname = "UpdateTrade")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Trade successfully updated"),
            @ApiResponse(code = 304, message = "Trade not updated")})
    @PutMapping("/{tradeId}")
    public ResponseEntity updateTrade(@PathVariable int tradeId, @RequestBody Trade trade) {
        Trade updatedTrade = service.updateTrade(tradeId, trade);
        return ResponseEntity.status(HttpStatus.OK).body(updatedTrade);
    }
}
