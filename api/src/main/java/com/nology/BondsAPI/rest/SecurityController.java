package com.nology.BondsAPI.rest;

import com.nology.BondsAPI.entity.Security;
import com.nology.BondsAPI.entity.Trade;
import com.nology.BondsAPI.service.SecurityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = "/api/securities", produces = MediaType.APPLICATION_JSON_VALUE)
public class SecurityController {

    @Autowired
    SecurityService service = new SecurityService();
    @Value("classpath:static/data.json")
    private Resource data;

    @ApiOperation(value = "Returns all securities", notes = "Get all securities", nickname = "allSecs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Securities successfully returned"),
            @ApiResponse(code = 404, message = "No securities found")})
    @GetMapping("/pages")
    public ResponseEntity<Page<Security>> getAllSecurities(@RequestParam(defaultValue = "0") int page,
                                                           @RequestParam(defaultValue = "10") int size) {
        Page<Security> securities = service.findAllSecurities(page, size);
        return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*").body(securities);
    }

    @ApiOperation(value = "Returns security for given ID", notes = "Get security by Id", nickname = "SecById")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Securities successfully returned"),
            @ApiResponse(code = 404, message = "No securities found for this ID"),
            @ApiResponse(code = 400, message = "Invalid security ID")})
    @GetMapping("/{id}")
    public ResponseEntity<Security> getSecurity(@PathVariable Integer id) {
        Security security = service.findSecurity(id);
        return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*").body(security);
    }

    @ApiOperation(value = "Returns all securities, optionally filtered for a given date (using start date parameter) or date range using both parameters", notes = "Get security by date", nickname = "allSecByDate")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Securities successfully returned"),
            @ApiResponse(code = 404, message = "No securities found within this date range"),
            @ApiResponse(code = 400, message = "Invalid inputs")})
    @GetMapping
    public ResponseEntity<List<Security>> getSecurityByDateRange(@RequestParam(required = false) LocalDate startDate, @RequestParam(required = false) LocalDate endDate) {
        List<Security> securities;
        if (startDate == null) {
            securities = service.findAllSecurities();
        } else if (endDate == null) {
            securities = service.getSecurityByDate(startDate);
        } else {
            securities = service.getSecurityByDateRange(startDate, endDate);
        }
        return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*").body(securities);
    }

    @ApiOperation(value = "Returns all trades for a given security ID", notes = "Get all trades", nickname = "allSecTrades")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Trades successfully returned"),
            @ApiResponse(code = 404, message = "Trades not found for this security ID")})
    @GetMapping("/{id}/trades")
    public ResponseEntity<List<Trade>> getTradesForASecurity(@PathVariable int id) {
        List<Trade> listOfTrades = service.findTradesForSecurity(id);
        return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*").body(listOfTrades);
    }

    @ApiOperation(value = "Returns securities for a given book ID", notes = "Get Security by Book", nickname = "SecByBookId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Securities successfully found"),
            @ApiResponse(code = 404, message = "No securities found for this book")})
    @GetMapping("/users")
    public ResponseEntity<Page<Security>> getSecuritiesByUser(@RequestParam String userEmail,
                                                              @RequestParam(defaultValue = "0") int page,
                                                              @RequestParam(defaultValue = "10") int size) {
        Page<Security> securities = service.findSecuritiesByUser(userEmail, page, size);
        return ResponseEntity.status(HttpStatus.OK).header("Access-Control-Allow-Origin", "*").body(securities);
    }

    @ApiOperation(value = "Creates a new security with provided inputs", notes = "Create new Security", nickname = "CreateSec")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Security successfully created"),
            @ApiResponse(code = 404, message = "Failed to create security"),
            @ApiResponse(code = 400, message = "Invalid input - checks parameters and data types")})
    @PostMapping()
    public ResponseEntity<Security> createNewSecurity(@Valid @RequestBody Security newSecurity) {
        Security security = service.createSecurity(newSecurity);
        return ResponseEntity.status(HttpStatus.OK).body(security);
    }

    @ApiOperation(value = "Updates a given security with provided inputs", notes = "Update Security", nickname = "UpdateSec")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Security successfully updated"),
            @ApiResponse(code = 304, message = "Failed to update security"),
            @ApiResponse(code = 400, message = "Invalid input - checks parameters and data types")})
    @PutMapping("/{id}")
    public ResponseEntity<Security> updateSecurity(@RequestBody Security updatedSecurity, @PathVariable Integer id) {
        Security security = service.updateSecurity(id, updatedSecurity);
        return ResponseEntity.status(HttpStatus.OK).body(security);
    }

    @ApiOperation(value = "Deletes a given security based on ID", notes = "Delete Security", nickname = "DeleteSec")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Security successfully deleted"),
            @ApiResponse(code = 404, message = "Failed to delete security")})
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSecurity(@PathVariable Integer id) {
        service.deleteSecurity(id);
        return ResponseEntity.status(HttpStatus.OK).body("security id: " + id + " deleted");
    }
}
