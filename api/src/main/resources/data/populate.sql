use bonds;

/* Insert data into the 'book' table. The names of the books are 'book1', 'book2' etc. */
INSERT INTO book (name) VALUES("book1");
INSERT INTO book (name) VALUES("book2");
INSERT INTO book (name) VALUES("book3");
INSERT INTO book (name) VALUES("book4");
INSERT INTO book(name) VALUES("Book5-no trades");

/* Insert data into the 'counterparty' table. The names of the counterparties are 'UBS', 'HSBC' etc. */
INSERT INTO counterparty (name) VALUES("UBS");
INSERT INTO counterparty (name) VALUES("HSBC");
INSERT INTO counterparty (name) VALUES("Goldman Sachs");
INSERT INTO counterparty (name) VALUES("Barclays");

/** Insert data into the 'user' table.
Eg. First user has name 'Oksana', email 'Oksana@oksana.com', and has role "Trader" **/
INSERT INTO user (name, email, role) VALUES ("Oksana", "Oksana@oksana.com", "Trader");
INSERT INTO user (name, email, role) VALUES ("Priya", "Priya@priya.com", "Ops");
INSERT INTO user (name, email, role) VALUES ("Tesh", "Tesh@tesh.com", "Trader");
INSERT INTO user (name, email, role) VALUES("Armity", "armity@armity.com", "Ops");

/** Insert data into the book_user table.
Eg. book_id of 1 is associated with the user_id of 1.
Remember that book_id and user_id are auto-incremented values as specified in schema.sql **/
INSERT INTO book_user (book_id, user_id) VALUES(1,1);
INSERT INTO book_user (book_id, user_id) VALUES(1,2);
INSERT INTO book_user (book_id, user_id) VALUES(1,3);
INSERT INTO book_user (book_id, user_id) VALUES(2,2);
INSERT INTO book_user (book_id, user_id) VALUES(3,2);
INSERT INTO book_user (book_id, user_id) VALUES(3,3);
INSERT INTO book_user (book_id, user_id) VALUES(4,1);
INSERT INTO book_user (book_id, user_id) VALUES(4,2);
INSERT INTO book_user (user_id, book_id) VALUES(4, 5);

/* Insert data into the securities table */
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356111",123456780,"Bundesbank",'2021/08/23', "Municipal", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356112",123456780,"Bundesbank",'2021/08/27', "Sovereign", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356113",123456780,"Bundesbank",'2021/08/26', "Sovereign", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356114",123456780,"Bundesbank",'2021/08/27', "Sovereign", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356115",123456780,"Bundesbank",'2021/08/20', "Municipal", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356116",123456780,"Bundesbank",'2021/08/27', "Government", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356117",123456780,"Bundesbank",'2021/08/27', "Government", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356118",123456780,"Bundesbank",'2021/08/27', "Corporate", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("A12356119",123456780,"Bundesbank",'2021/08/27', "Agency", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("B12356111",123456780,"Bundesbank",'2021/08/29', "Agency", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("B12356112",123456780,"Bundesbank",'2021/08/29', "Government", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("B12356113",123456780,"Bundesbank",'2021/08/29', "Corporate", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("B12356114",123456780,"Bundesbank",'2021/08/29', "Corporate", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("B12356115",123456780,"Bundesbank",'2021/08/23', "Corporate", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("B12356113",123456780,"Bundesbank",'2021/08/23', "Corporate", 2.0, 900, "active", "USD" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("GBP1235647",4443456780,"Tesco",'2021/08/21', "Corporate", 1.0, 1500, "active", "GBP" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("X1245670001",4455456780,"Volkswagen",'2021/08/21', "Municipal", 6.75, 1570, "active", "EUR" );
INSERT INTO security (isin, cusip, issuer_name, maturity_date, type, coupon, face_value, status, currency)
VALUES ("US124567899",3366456785,"Fred Bank",'2021/08/21', "Agency", 2.50, 900, "active", "GBP" );

/* Insert data into trades table */
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (1, 1, 1,"USD", "open", 50, 90, "buy", '2020/05/13', '2021/08/23');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (1, 1, 1,"USD", "open", 50, 90, "Sell", '2020/05/13', '2021/08/23');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (1, 1, 1,"USD", "open", 50, 90, "buy", '2020/05/13', '2021/08/23');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 2, 3,"GBP", "open", 60, 70.5, "buy", '2021/06/04', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 2, 3,"GBP", "open", 30, 70.5, "buy", '2021/06/04', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 2, 3,"GBP", "open", 40, 70.5, "buy", '2021/06/04', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 2, 3,"GBP", "open", 20, 70.5, "buy", '2021/06/16', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (1, 2, 3,"GBP", "open", 50, 70.5, "buy", '2021/06/22', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (1, 3, 2,"TRY", "closed", 40, 100, "buy", '2019/02/14', '2021/07/12');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 2, 3,"GBP", "open", 10, 70.5, "buy", '2021/06/05', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (1, 2, 3,"GBP", "open", 5, 70.5, "buy", '2021/06/04', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 2, 3,"GBP", "open", 50, 70.5, "buy", '2021/06/22', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 3, 2,"TRY", "inopen", 40, 100, "buy", '2019/02/14', '2021/07/12');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 2, 3,"GBP", "open", 10, 70.5, "buy", '2021/06/05', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 2, 3,"GBP", "open", 5, 70.5, "buy", '2021/06/04', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 2, 3,"GBP", "open", 50, 70.5, "buy", '2021/06/22', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (3, 3, 2,"TRY", "closed", 40, 100, "buy", '2019/02/14', '2021/07/12');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 2, 3,"GBP", "open", 10, 70.5, "buy", '2021/06/05', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 2, 3,"GBP", "open", 5, 70.5, "buy", '2021/06/04', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 4, 3,"GBP", "open", 50, 70.5, "buy", '2021/06/22', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (3, 7, 2,"TRY", "open", 40, 100, "buy", '2019/02/14', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 8, 3,"GBP", "open", 10, 70.5, "buy", '2021/06/05', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 9, 3,"GBP", "open", 5, 70.5, "buy", '2021/06/04', '2021/08/27');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 10, 3,"GBP", "open", 50, 70.5, "buy", '2021/06/22', '2021/08/29');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (3, 10, 2,"TRY", "open", 40, 100, "buy", '2019/02/14', '2021/08/29');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 10, 3,"GBP", "open", 10, 70.5, "buy", '2021/06/05', '2021/08/29');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 11, 3,"GBP", "open", 5, 70.5, "buy", '2021/06/04', '2021/08/29');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 11, 3,"GBP", "open", 50, 70.5, "buy", '2021/06/22', '2021/08/29');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (3, 12, 2,"TRY", "open", 40, 100, "buy", '2019/02/14', '2021/07/19');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 13, 3,"GBP", "open", 10, 70.5, "buy", '2021/06/05', '2021/08/29');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 9, 3,"GBP", "open", 5, 70.5, "buy", '2021/06/04', '2021/08/29');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (4, 3, 2,"EUR", "closed", 30, 120.2, "sell", '2021/05/04', '2027/04/21');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (1, 3, 4,"MXN", "closed", 10, 75.5, "buy", '2020/06/04', '2026/04/21');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 3, 3,"PLN", "closed", 20, 120, "sell", '2021/07/04', '2027/04/21');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (2, 1, 1,"USD", "closed", 45, 80, "buy", '2018/02/04', '2022/04/07');
INSERT INTO trades (book_id, security_id, counterparty_id, currency, status, quantity, unit_price, buy_sell, trade_date, settlement_date)
VALUES (3, 1, 2,"EUR", "closed", 61, 140, "sell", '2017/03/04', '2022/04/09');