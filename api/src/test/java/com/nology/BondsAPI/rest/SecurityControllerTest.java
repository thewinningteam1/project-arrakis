package com.nology.BondsAPI.rest;

import com.nology.BondsAPI.entity.Book;
import com.nology.BondsAPI.entity.Counterparty;
import com.nology.BondsAPI.entity.Security;
import com.nology.BondsAPI.entity.Trade;
import com.nology.BondsAPI.exception.BookNotFoundException;
import com.nology.BondsAPI.exception.SecurityNotFoundException;
import com.nology.BondsAPI.exception.UserNotFoundException;
import com.nology.BondsAPI.service.SecurityService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(SecurityController.class)
public class SecurityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SecurityService service;

    private final List<Security> securitiesList = List.of(
            new Security("X124567880", "123456789", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567882", "123456799", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567883", "123456722", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567884", "123456755", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567881", "123456789", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567882", "123456799", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567883", "123456722", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567884", "123456755", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567884", "123456755", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR"),
            new Security("X124567884", "123456755", "Bundesbank", LocalDate.of(2020, 11, 03), "Sovereign", 2.0, 1250, "Active", "EUR")
    );
    private final List<Trade> trades = List.of(

            new Trade(1, "GBP", "active", 90, 25.5, "buy",
                    LocalDate.of(2021, 03, 12), LocalDate.of(2028, 03, 11)),
            new Trade(2, "USD", "active", 100, 25.5, "buy",
                    LocalDate.of(2021, 04, 11), LocalDate.of(2023, 04, 10))
    );
    private final int id = 1;

    private Page<Security> securities =new PageImpl<Security>(List.of(

            new Security(1,
                    "Isin1",
                    "Cusip1",
                    "Issuer1",
                    LocalDate.of(2021, 9,5),
                    "Municipal",
                    5.6,
                    1000,
                    "Active",
                    "EUR"),
            new Security(2,
                         "Isin2",
                         "Cusip2",
                         "Issuer2",
                         LocalDate.of(2021, 7,5),
                         "Municipal",
                         5.6,
                         1000,
                         "Active",
                         "EUR")

    ).stream().sorted(Comparator.comparing(Security::getMaturityDate))
            .collect(Collectors.toCollection(LinkedHashSet::new))
            .stream().collect(Collectors.toList()));


    Security security = new Security(1, "GBP1234567", "1234567", "Tesco", LocalDate.of(2024,12, 22), "Corporate", 6.75, 900, "Active", "GBP");
    Security security1 = new Security(2, "USD1234567", "2234567", "Toyota", LocalDate.of(2028,11, 12), "Corporate", 7.25, 600, "Active", "GBP");
    Counterparty counterparty = new Counterparty(1, "Wallmart");
    Counterparty counterparty2 = new Counterparty(2, "Co-op");
    Book book1 = new Book(1, "ARMFEET");
    Book book2 = new Book(2, "LEGFEET");


    private final List<Trade> trades1 = List.of(

            new Trade( 1, security1, book1, counterparty, "GBP","active", 90, 25.5, "buy",
                    LocalDate.of(2021,03,12),  LocalDate.of(2028,03, 11)),
            new Trade( 2, security, book2, counterparty2,"USD", "active", 100, 25.5, "buy",
                    LocalDate.of(2021,04,11), LocalDate.of(2023,04, 10))
    );



    @Test
    public void indexRouteShouldReturnListOfSecuritiesFromService() throws Exception {
        when(service.findAllSecurities()).thenReturn(securitiesList);
        this.mockMvc.perform(get("/api/securities"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(10)))
                .andExpect(jsonPath("$[0].isin", is("X124567880")))
                .andReturn();
    }

    @Test
    public void findSecuritiesForAUser() throws Exception {
        Mockito.when(service.findSecuritiesByUser(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt()))
                        .thenReturn(securities);
        this.mockMvc.perform(get("/api/securities/users?userEmail=test@test.com"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.content[0].isin", is("Isin2")))
                    .andExpect(jsonPath("$.content[1].isin", is("Isin1")))
                    .andExpect(jsonPath("$.content[0].maturityDate", is("05/07/2021")))
                    .andReturn();

    }


    @Test
    public void securitiesNotFound() throws Exception {
        Mockito.when(service.findSecuritiesByUser(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
                .thenThrow(new SecurityNotFoundException("No securities found for user test@test.com"));

        this.mockMvc.perform(get("/api/securities/users?userEmail=test@test.com"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("No securities found for user test@test.com")))
                .andReturn();

    }

    @Test
    public void booksNotFound() throws Exception {
        Mockito.when(service.findSecuritiesByUser(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt()))
                .thenThrow(new BookNotFoundException("No books found for user test@test.com"));

        this.mockMvc.perform(get("/api/securities/users?userEmail=test@test.com"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("No books found for user test@test.com")))
                .andReturn();

    }

    @Test
    public void userNotFound() throws Exception {
        Mockito.when(service.findSecuritiesByUser(Mockito.anyString(),Mockito.anyInt(),Mockito.anyInt()))
                .thenThrow(new UserNotFoundException("No user found with test@test.com"));

        this.mockMvc.perform(get("/api/securities/users?userEmail=test@test.com"))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("No user found with test@test.com")))
                .andReturn();

    }

    @Test
    public void findTradesForSecurityRouteShouldReturnAllTradesForAGivenSecurity() throws Exception {
        when(service.findTradesForSecurity(anyInt())).thenReturn(trades);
        this.mockMvc.perform(get("/api/securities/"+id+"/trades")) // in this context, your id is a variable - defined in controller as {id}.  No spaces.
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2))) // whole object is referred to as $ - e.g. in this context, list of 2 items.
                .andReturn();
    }

}
