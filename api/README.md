# Arrakis API

Arrakis/Bonds is a tool for bond redemption management. It increases the efficiency and clarity with which operations users can manage their books and clients.

Arrakis API is written in Java with a Spring Boot backend and a MySQL database. The project is deployed to Google Cloud with a ~~CI/CD pipeline in place~~. 

This project was developed as part of the Nology 2021 bootcamp using best software engineering practices.

### Development build

You will need to have Java 11 installed and a bash-like shell.

* Go to the repository in GitHub and select the master branch. Click the 'Code' button and copy the HTTPS link
* Using the local machine terminal, run a git clone command to copy the repository to your machine.  Insert the HTTPS link copied from the GitHub repository in terminal e.g: git clone https://github.com/nology-tech/arrakis-api.git^
* Open the project in your IDE of choice (we recommend IntelliJ), and wait for it to load all the dependencies
* Create your local database using the provided schema and populate scripts. Make sure to update the dates of bonds and trades to future dates in order to use the API
* Build and run the app directly from IntelliJ to start the development server

### Cloud deployment

add steps to deploy to the cloud

### Architecture

#### Data

The main data entity the API handles are Book, Counterparty, Security, Trade and User. See the database diagram:

![img.png](img.png)

The database is currently populated based on pre-specified script. Moving forwards, we would like to integrate with another system to consume production bonds data (see Future features section below).

#### Rest API
Data is exposed to consumers via a REST API. 
There are two main controllers handling requests: SecurityController and TradeController.

SecurityController provides the following functionality:
* Get all securities
* Get securities by ID, date range or corresponding user ID
* Get all trades for a security
* Update, create and delete a security

TradeController provides the following functionality:
* Get trade by ID
* Retrieve security to which the trade belongs
* Create and update a trade

For more detailed information on the REST endpoints, see the Swagger API section below.

#### Known issues

* The front-end server currently does not handle situation when the API server might be down
* There might be performance issues when getting all securities by date range, if the data set becomes too large. The implementation might need to be reviewed for optimisation in the future

### Swagger API and monitoring

Monitoring is provided through Spring Actuator. See the /actuator endpoint to check app stats and logs.

To see the full documentation on all API endpoints, navigate to the /swagger-ui/ endpoint.

### Future features

* Consume real-world data from the relevant bonds database/API
* Implement more comprehensive error handling

### Licence

?
